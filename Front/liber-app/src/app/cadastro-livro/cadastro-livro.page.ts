import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-cadastro-livro',
  templateUrl: './cadastro-livro.page.html',
  styleUrls: ['./cadastro-livro.page.scss'],
})
export class CadastroLivroPage implements OnInit {

  bookRegisterForm: FormGroup;

  constructor(public formbuilder: FormBuilder) { 
    this.bookRegisterForm = this.formbuilder.group({
      title: [null, [Validators.required]],
      author: [null, [Validators.required]],
      resume:[null],
      publisher:[null,  [Validators.required]],
      volume:[null,  [Validators.required]],
      condition:[null, [Validators.required]],
      price:[null,[Validators.required]]
    });
  }

  ngOnInit() {
  }

  submitForm(form){
    console.log(form);
    console.log(form.value);
  }

}
