import { Component, OnInit } from '@angular/core';
import { async } from '@angular/core/testing';
import { HammerGestureConfig } from '@angular/platform-browser';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

export class Livros{
  title: string;
  photo: string;
  author: string;
  resume: string;
  publisher: string;
  condition: string;
  volumes: string;
  price: string;
}


@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  livros: Livros[];

  livros2: Livros[];

  livros3: Livros[];

  livros4: Livros[];

  livros5: Livros[];

  slideOpts = {slidesPerView: 3}


  constructor(private router: Router) { }

  redirecionar(){
    this.router.navigate(['/livro'])
  }

  ngOnInit() {
    this.livros = [
      {
        title: 'Box Agatha Christie',
        photo: '../../assets/imagens/imagem1.png',
        author: 'Agatha Christie',
        resume: '.',
        publisher: 'HarperCollins',
        condition: 'Usado',
        volumes: '6 volumes',
        price: 'R$80'
      },
      {
        title: 'Box Alice no País das Maravilhas',
        photo:'../../assets/imagens/imagem3.png',
        author: 'Lewis Carrol',
        resume:'.',
        publisher: 'Pandorga',
        condition: 'Seminovo',
        volumes: '3 volumes',
        price: 'R$45'
      },
      {
        title: 'Box Anne de Green Gables',
        photo:'../../assets/imagens/imagem2.png',
        author: ' Lucy Maud Montgomery',
        resume: '.',
        publisher: 'Autêntica infantil e juvenil',
        condition: 'Novo',
        volumes: '3 volumes',
        price: 'R$60'
      },
      {
        title: 'Box Corte De Espinhos e Rosas',
        photo:'../../assets/imagens/imagem7.png',
        author: 'Sarah J. Maas',
        resume: '.',
        publisher: 'Galera',
        condition: 'Seminovo',
        volumes: '4 volumes',
        price: 'R$160'
      },
      {
        title: 'Box Percy Jackson e os Olimpianos',
        photo:'../../assets/imagens/imagem5.png',
        author: 'Rick Riordan',
        resume: '.',
        publisher: 'Intrínseca',
        condition: 'Novo',
        volumes: '5 volumes',
        price: 'R$120'
      }
    ]
    
    this.livros2 = [
      {
        title: 'Torto Arado',
        photo: '../../assets/imagens/livro1.png',
        author: 'Itamar Vieira Junior',
        resume: 'Nas profundezas do sertão baiano, as irmãs Bibiana e Belonísia encontram uma velha e misteriosa faca na mala guardada sob a cama da avó. Ocorre então um acidente. E para sempre suas vidas estarão ligadas ― a ponto de uma precisar ser a voz da outra. Numa trama conduzida com maestria e com uma prosa melodiosa, o romance conta uma história de vida e morte, de combate e redenção.',
        publisher: 'Todavia',
        condition: 'Usado',
        volumes: '1 volume',
        price: 'R$35,00'
      },
      {
        title: 'A revolução dos bichos',
        photo: '../../assets/imagens/livro2.jpg',
        author: 'George Orwell',
        resume: 'Num belo dia, os animais da fazenda do sr. Jones se dão conta da vida indigna a que são submetidos: eles se matam de trabalhar para os homens, lhes dão todas as suas energias em troca de uma ração miserável, para ao final serem abatidos sem piedade. Liderados por um grupo de porcos, os bichos então expulsam o fazendeiro de sua propriedade e pretendem fazer dela um Estado em que todos serão iguais.',
        publisher: 'Companhia das Letras',
        condition: 'Seminovo',
        volumes: '1 volume',
        price: 'R$15,00'
      },
      {
        title: 'Bridgerton: O Duque & Eu',
        photo: '../../assets/imagens/livro3.png',
        author: 'Julia Quinn',
        resume: 'Primeiro dos nove livros da série Os Bridgertons, O duque e eu é uma bela história sobre o poder do amor, contada com o senso de humor afiado e a sensibilidade que são marcas registradas de Julia Quinn.',
        publisher: 'GMT',
        condition: 'Novo',
        volumes: '1 volume',
        price: 'R$29,00'
      },
      {
        title: 'As Crônicas de Nárnia',
        photo: '../../assets/imagens/livro4.jpg',
        author: 'C. S. Lewis',
        resume: 'Viagens ao fim do mundo, criaturas fantásticas e batalhas épicas entre o bem e o mal. Este livro apresenta as sete crônicas de Nárnia integralmente, num único volume. Os livros são apresentados de acordo com a ordem de preferência do autor, cada capítulo com uma ilustração do artista Pauline Baynes. ´As crônicas de Nárnia´ apresenta aventuras, personagens e fatos que falam aos leitores de todas as idades.',
        publisher: 'Martins Fontes',
        condition: 'Usado',
        volumes: 'Volume único',
        price: 'R$32,00'
      },
      {
        title: 'O Diário de Anne Frank',
        photo: '../../assets/imagens/livro5.jpeg',
        author: 'Anne Frank',
        resume: ' Assim podemos descrever os relatos feitos por Anne em seu diário. Isolados do mundo exterior, os Frank enfrentaram a fome, o tédio e a terrível realidade do confinamento, além da ameaça constante de serem descobertos. Nas páginas de seu diário, Anne Frank registra as impressões sobre esse longo período no esconderijo. Alternando momentos de medo e alegria, as anotações se mostram um fascinante relato sobre a coragem e a fraqueza humanas e, sobretudo, um vigoroso autorretrato de uma menina sensível e determinada, cuja vida foi tragicamente interrompida.',
        publisher: 'Record',
        condition: 'Seminovo',
        volumes: '1 volume',
        price: 'R$12,00'
      },
      {
        title: 'Harry Potter e a Ordem da Fênix',
        photo: '../../assets/imagens/livro6.jpg',
        author: 'J.K. Rowling',
        resume: 'Neste quinto livro da saga, o protagonista, numa crise típica da adolescência, tem ataques de mau humor com a perseguição da imprensa, que o segue por todos os lugares e chega a inventar declarações que nunca deu. Harry vai enfrentar as investidas de Voldemort sem a proteção de Dumbledore, já que o diretor de Hogwarts é afastado da escola. E vai ser sem a ajuda de seu protetor que o jovem herói enfrentará descobertas sobre a personalidade controversa de seu pai, Tiago Potter, e a morte de alguém muito próximo.',
        publisher: 'Rocco',
        condition: 'Seminovo',
        volumes: '1 volume',
        price: 'R$48,50'
      }

    ]

    this.livros3 = [
      {
        title: 'O Extraordinário',
        photo: '../../assets/imagens/livro9.jpg',
        author: 'R. J. Palacio',
        resume: 'August Pullman, o Auggie, nasceu com uma síndrome cuja sequela é uma severa deformidade facial, que lhe impôs diversas cirurgias e complicações médicas. Por isso ele nunca frequentou uma escola de verdade.. até agora. Todo mundo sabe que é difícil ser um aluno novo, mais ainda quando se tem um rosto tão diferente. Prestes a começar o quinto ano em um colégio particular em Nova York, Auggie tem uma missão nada fácil pela frente: convencer os colegas de que, apenas da aparência incomum, ele é um menino igual a todos os outros.',
        publisher: 'Intrínseca',
        condition: 'Usado',
        volumes: '1 volume',
        price: 'R$25,00'
      },
      {
        title: 'eu tenho sérios poemas mentais',
        photo: '../../assets/imagens/livro10.jpg',
        author: 'Pedro Salomão',
        resume: 'Antes de mais nada eu gostaria de pedir licença ao seu coração, pois sinto que a relação que vamos criar a partir de agora é muito forte. Eu escrevi neste livro as poesias sobre os lugares mais íntimos em mim, aqueles lugares que são tão profundos que até eu mesmo só consigo visitar às vezes... Estou me apresentando para você como sou, sem rosto, sem voz e sem cheiro. Apenas ideias. E tudo o que sou são ideias. Neste momento, uma voz está lendo estas palavras em sua cabeça, dentro da sua imaginação, e já não é mais a minha voz, eu não sei como ela é, mas espero que seja doce e suave. Seus pensamentos estão dançando com os meus, e já não sei mais onde eu termino e você começa, e esta é a relação mais íntima que eu já tive com alguém. Obrigado por estar aqui.',
        publisher: 'Outro Planeta',
        condition: 'Novo',
        volumes: '1 volume',
        price: 'R$21,99'
      },
      {
        title: 'Capitães da Areia',
        photo: '../../assets/imagens/livro11.png',
        author: 'Jorge Amado',
        resume: ' A obra retrata a vida de um grupo de menores abandonados, que crescem nas ruas da cidade de Salvador, Bahia, vivendo em um trapiche, roubando para sobreviver, chamados de "Capitães da Areia". O livro forma parte do movimento da Romance de 30, marcando uma mudança do modernismo da década anterior, passando de experimentação literária para um engajamento com questões sociais.',
        publisher: 'José Olympio',
        condition: 'Usado',
        volumes: '1 volume',
        price: 'R$15,00'
      },
      {
        title: 'Um Mistério no Caribe',
        photo: '../../assets/imagens/livro12.jpg',
        author: 'Agatha Christie',
        resume: 'Miss Marple está entediada em suas férias à beira-mar. O sol até ajuda com seu reumatismo, mas nada divertido acontece. Até que finalmente seu interesse é despertado pela história de um assassinato contada pelo Major Palgrave, mas, justo quando ele ia lhe mostrar uma foto do homicida, o major se interrompe. No dia seguinte, é encontrado morto em seu quarto, sem nunca ter terminado a história. Sua morte, aparentemente, é apenas a primeira dos muitos mistérios que o Caribe apresenta?',
        publisher: 'Harper Collins',
        condition: 'Seminovo',
        volumes: '1 volume',
        price: 'R$31,00'
      },
      {
        title: 'Pessoas Normais',
        photo: '../../assets/imagens/livro13.jpg',
        author: 'Sally Rooney',
        resume: 'Na escola, no interior da Irlanda, Connell e Marianne fingem não se conhecer. Ele é a estrela do time de futebol, ela é solitária e preza por sua privacidade. Mas a mãe de Connell trabalha como empregada na casa dos pais de Marianne, e quando o garoto vai buscar a mãe depois do expediente, uma conexão estranha e indelével cresce entre os dois adolescentes – contudo, um deles está determinado a esconder a relação. Um ano depois, ambos estão na universidade, em Dublin. Marianne encontrou seu lugar em um novo mundo enquanto Connell fica à margem, tímido e inseguro. Ao longo dos anos da graduação, os dois permanecem próximos, como linhas que se encontram e separam conforme as oportunidades da vida. Porém, enquanto Marianne se embrenha em um espiral de autodestruição e Connell começa a duvidar do sentido de suas escolhas, eles precisam entender até que ponto estão dispostos a ir para salvar um ao outro. Uma história de amor entre duas pessoas que tentam ficar separadas, mas descobrem que isso pode ser mais difícil do que tinham imaginado.',
        publisher: 'Companhia das Letras',
        condition: 'Novo',
        volumes: '1 volume',
        price: 'R$43,00'
      },
      {
        title: 'Por Lugares Incríveis',
        photo: '../../assets/imagens/livro14.jpg',
        author: 'Jennifer Niven',
        resume: 'Violet Markey tinha uma vida perfeita, mas todos os seus planos deixam de fazer sentido quando ela e a irmã sofrem um acidente de carro e apenas Violet sobrevive. Sentindo-se culpada pelo que aconteceu, a garota se afasta de todos e tenta descobrir como seguir em frente. Theodore Finch é o esquisito da escola, perseguido pelos valentões e chamado de “aberração” por onde passa. Para piorar, é obrigado a lidar com longos períodos de depressão, o pai violento e a apatia do resto da família. Enquanto Violet conta os dias para o fim das aulas, quando poderá ir embora da cidadezinha onde mora, Finch pesquisa diferentes métodos de suicídio e imagina se conseguiria levar algum deles adiante. Em uma dessas tentativas, ele vai parar no alto da torre da escola e, para sua surpresa, encontra Violet, também prestes a pular. Um ajuda o outro a sair dali, e essa dupla improvável se une para fazer um trabalho de geografia: conhecer lugares incríveis do estado onde moram. Ao lado de Finch, Violet para de contar os dias e finalmente passa a vivê-los. O garoto, por sua vez, encontra alguém com quem pode ser ele mesmo, e torce para que consiga se manter desperto.',
        publisher: 'Seguinte',
        condition: 'Usado',
        volumes: '1 volume',
        price: 'R$23,89'
      }
    ]

    this.livros4 = [
      {
        title: 'Persépolis',
        photo: '../../assets/imagens/livro17.jpg',
        author: 'Marjane Satrapi',
        resume: 'Marjane Satrapi tinha apenas dez anos quando se viu obrigada a usar o véu islâmico, numa sala de aula só de meninas. Nascida numa família moderna e politizada, em 1979 ela assistiu ao início da revolução que lançou o Irã nas trevas do regime xiita - apenas mais um capítulo nos muitos séculos de opressão do povo persa. Vinte e cinco anos depois, com os olhos da menina que foi e a consciência política à flor da pele da adulta em que se transformou, Marjane emocionou leitores de todo o mundo com essa autobiografia em quadrinhos, que só na França vendeu mais de 400 mil exemplares. Em Persépolis, o pop encontra o épico, o oriente toca o ocidente, o humor se infiltra no drama - e o Irã parece muito mais próximo do que poderíamos suspeitar.',
        publisher: 'Quadrinhos na Cia',
        condition: 'Usado',
        volumes: '1 volume',
        price: 'R$38,00'
      },
      {
        title: 'O Princípe e a Costureira',
        photo: '../../assets/imagens/livro18.jpg',
        author: 'Jen Wang',
        resume: 'Sebastian é o príncipe herdeiro da Bélgica. Ele está em busca de uma esposa ― ou melhor, seus pais estão cuidando disso para ele. Sebastian, na verdade, está mais ocupado escondendo seu segredo de todos: à noite, ele coloca vestidos ousados e sai pelas ruas de Paris como a fabulosa Lady Crystallia, o ícone fashion da capital da moda. Tal façanha é graças ao belíssimo trabalho de Frances, sua melhor amiga e costureira, e uma das duas únicas pessoas que sabem a verdade. Mas Frances sonha com a grandeza e o reconhecimento, e fazer os vestidos de Lady Crystallia significa viver à sombra de um segredo para sempre…',
        publisher: 'Darkside',
        condition: 'Seminovo',
        volumes: '1 volume',
        price: 'R$50,00'
      },
      {
        title: 'MAUS',
        photo: '../../assets/imagens/livro19.jpg',
        author: 'Art Spiegelman',
        resume: ' Nas tiras, os judeus são desenhados como ratos e os nazistas ganham feições de gatos; poloneses não-judeus são porcos e americanos, cachorros. Esse recurso, aliado à ausência de cor dos quadrinhos, reflete o espírito do livro: trata-se de um relato incisivo e perturbador, que evidencia a brutalidade da catástrofe do Holocausto. Spiegelman, porém, evita o sentimentalismo e interrompe algumas vezes a narrativa para dar espaço a dúvidas e inquietações. É implacável com o protagonista, seu próprio pai, retratado como valoroso e destemido, mas também como sovina, racista e mesquinho. De vários pontos de vista, uma obra sem equivalente no universo dos quadrinhos e um relato histórico de valor inestimável.',
        publisher: 'Quadrinhos na Cia',
        condition: 'Novo',
        volumes: '1 volume',
        price: 'R$42,90'
      },
      {
        title: 'Grama',
        photo: '../../assets/imagens/livro20.jpg',
        author: 'Keum Suk Gendry-Kim',
        resume: 'Grama é uma poderosa graphic novel antiguerra que narra a história real da sul-coreana Ok-sun Lee, vendida pela própria família na infância e forçada à escravidão sexual pelo Exército Imperial Japonês. Ela é uma das várias mulheres que foram capturadas para servir aos soldados nas chamadas “casas de conforto”, espalhadas pela China e por territórios ocupados pelo Japão durante a Segunda Guerra Sino-Japonesa e a Segunda Guerra Mundial, em um dos episódios mais vergonhosos do passado da humanidade. Ok-sun Lee, hoje com mais de 90 anos, se tornou uma importante ativista pela indenização das “mulheres de conforto”, e é por meio de seus relatos à autora Keum Suk Gendry-Kim que acompanhamos sua triste história de vida.',
        publisher: 'Pipoca e Nanquim',
        condition: 'Seminovo',
        volumes: '1 volume',
        price: 'R$70,00'
      },
      {
        title: 'O Mundo de Aisha',
        photo: '../../assets/imagens/livro21.jpg',
        author: ' Ugo Bertotti',
        resume: 'Obrigadas a se casarem ainda meninas. Escravizadas, violentadas, por vezes assassinadas. Cobertas com o véu negro – o niqab – as mulheres do Iêmen parecem fantasmas. Contudo, pouco a pouco, com delicadeza, coragem e determinação, elas travam uma batalha corajosa por sua emancipação. Uma revolução silenciosa está em marcha para fazer valer seus direitos e sua liberdade. Aisha, Sabiha, Hamedda, Houssen e tantas outras: aqui estão algumas de suas histórias. Uma extraordinária reportagem em quadrinhos de Ugo Bertotti inspirada pelas imagens e pelas entrevistas da fotojornalista Agnes Montanari.',
        publisher: 'Nemo',
        condition: 'Novo',
        volumes: '1 volume',
        price: 'R$25,00'
      },
      {
        title: 'Scott Pilgrim Contra O Mundo 2',
        photo: '../../assets/imagens/livro22.jpg',
        author: 'Bryan Lee Malley',
        resume: 'A vida de Scott Pilgrim parece estar se acertando. De saída, ele já mandou para a lona dois dos ex-namorados do mal de Ramona Flowers (faltam cinco). Além disso, o namoro com a misteriosa americana parece estar engrenando, e até mesmo sua banda, a Sex Bob-Omb, tem conseguido acertar um ou dois acordes. Claro que, num país tão tumultuado quanto o Canadá, as coisas nunca são fáceis, e o período de relativa tranquilidade é interrompido pela chegada da turnê do Clash at the Demon’s Head, a banda de rock mais incrivelmente poderosa de que se tem notícia. Não bastasse, o grupo é liderado por Envy Adams, ex-namorada de Scott, que o colocou numa espécie de coma emocional ao largá-lo, um ano e meio atrás. E o embate com o passado traz consigo um conflito de contornos mais épicos: o atual namorado de Envy Adams é ninguém menos que Todd Ingram, o vegan com poderes místicos que Scott precisa derrotar no seu caminho para o coração de Ramona. Dotado de poderes incríveis (“ele frequentou a academia vegan e tudo!”), Todd será o maior desafio de Pilgrim. Ou pelo menos é o que se imagina. No segundo capítulo deste volume, Scott irá descobrir outra faceta de sua enigmática namorada, será perseguido por ninjas e samurais e terá de lidar com os grandes dilemas da vida adulta: dividir ou não um apartamento com seu melhor amigo gay e, principalmente, arrumar um emprego ou continuar jogando Final Fantasy II.',
        publisher: 'Quadrinhos na Cia',
        condition: 'Usado',
        volumes: '1 volume',
        price: 'R$12,00'
      }
    ]

    this.livros5 = [
      {
        title: 'Mulheres de Cinzas',
        photo: '../../assets/imagens/livro25.jpg',
        author: 'Mia Couto',
        resume: 'Primeiro livro da trilogia As Areias do Imperador, Mulheres de cinzas é um romance histórico sobre a época em que o sul de Moçambique era governado por Ngungunyane (ou Gungunhane, como ficou conhecido pelos portugueses), o último dos líderes do Estado de Gaza - segundo maior império no continente comandado por um africano. Em fins do século XIX, o sargento português Germano de Melo foi enviado ao vilarejo de Nkokolani para a batalha contra o imperador que ameaçava o domínio colonial. Ali o militar encontra Imani, uma garota de quinze anos que aprendeu a língua dos europeus e será sua intérprete. Ela pertence à tribo dos VaChopi, uma das poucas que ousou se opor à invasão de Ngungunyane. Mas, enquanto um de seus irmãos lutava pela Coroa de Portugal, o outro se unia ao exército dos guerreiros do imperador africano. O envolvimento entre Germano e Imani passa a ser cada vez maior, malgrado todas as diferenças entre seus mundos. Porém, ela sabe que num país assombrado pela guerra dos homens, a única saída para uma mulher é passar despercebida, como se fosse feita de sombras ou de cinzas.',
        publisher: 'Companhia das Letras',
        condition: 'Seminovo',
        volumes: '1 volume',
        price: 'R$32,00'
      },
      {
        title: 'As Vantagens de Ser Invisível',
        photo: '../../assets/imagens/livro26.jpg',
        author: 'Stephen Chbosky',
        resume: 'Ao mesmo tempo engraçado e atordoante, o livro reúne as cartas de Charlie, um adolescente de quem pouco se sabe – a não ser pelo que ele conta ao amigo nessas correspondências –, que vive entre a apatia e o entusiasmo, tateando territórios inexplorados, encurralado entre o desejo de viver a própria vida e ao mesmo tempo fugir dela. As dificuldades do ambiente escolar, muitas vezes ameaçador, as descobertas dos primeiros encontros amorosos, os dramas familiares, as festas alucinantes e a eterna vontade de se sentir “infinito” ao lado dos amigos são temas que enchem de alegria e angústia a cabeça do protagonista em fase de amadurecimento.',
        publisher: 'Rocco',
        condition: 'Usado',
        volumes: '1 volume',
        price: 'R$25,00'
      },
      {
        title: 'Neuromancer',
        photo: '../../assets/imagens/livro27.jpg',
        author: 'William Gibson',
        resume: 'O Céu sobre o porto tinha cor de televisão num canal fora do ar. Considerada a obra precursora do movimento cyberpunk e um clássico da ficção científica moderna, Neuromancer conta a história de Case, um cowboy do ciberespaço e hacker da matrix. Como punição por tentar enganar os patrões, seu sistema nervoso foi contaminado por uma toxina que o impede de entrar no mundo virtual. Agora, ele vaga pelos subúrbios de Tóquio, cometendo pequenos crimes para sobreviver, e acaba se envolvendo em uma jornada que mudará para sempre o mundo e a percepção da realidade.',
        publisher: 'Editora Aleph',
        condition: 'Novo',
        volumes: '1 volume',
        price: 'R$42,00'
      },
      {
        title: 'Operação Perfeito',
        photo: '../../assets/imagens/livro28.jpg',
        author: 'Rachel Joyce',
        resume: 'Em uma manhã nebulosa de 1972, a vida de Byron Hemming, de 12 anos, muda de repente. Tudo acontece em menos de dois segundos, quando ele e a mãe se envolvem em um acidente de carro. Embora o garoto tenha certeza de que o acidente aconteceu, sua mãe age como se nada tivesse acontecido. Nos dias e nas semanas seguintes, Byron embarca em uma jornada para descobrir o que realmente houve naquela manhã que mudou sua vida. Junto com o amigo James, ele cria a Operação Perfeito, um conjunto de planos para tentar resolver a situação.',
        publisher: 'Suma',
        condition: 'Usado',
        volumes: '1 volume',
        price: 'R$29,90'
      },
      {
        title: 'Morte Súbita',
        photo: '../../assets/imagens/livro29.jpg',
        author: 'J.K. Rowling',
        resume: 'Pagford é, aparentemente, uma pacata cidade inglesa com tudo o de mais comum e organizado que pode haver, mas o que está por trás da fachada bonita é uma cidade em guerra - uma guerra de classes, credos, gerações e interesses.Ricos em guerra com os pobres, adolescentes em guerra com seus pais, esposas em guerra com seus maridos, professores em guerra com seus pupilos… Pagford não é o que parece ser. O assento vazio deixado por Barry no conselho municipal logo se torna o catalisador para a maior guerra que a cidade já viu. Quem triunfará em uma eleição repleta de duplicidade, paixão e revelações inesperadas?',
        publisher: 'Casa dos Livros',
        condition: 'Seminovo',
        volumes: '1 volume',
        price: 'R$43,00'
      },
      {
        title: '1984',
        photo: '../../assets/imagens/livro30.jpg',
        author: 'George Orwell',
        resume: 'Publicado em 1949, o texto de Orwell nasceu destinado à polêmica. Traduzido em mais de sessenta países, virou minissérie, filmes, quadrinhos, mangás e até uma ópera. Ganhou holofotes em 1999, quando uma produtora holandesa batizou seu reality show de Big Brother. 1984 foi responsável pela popularização de muitos termos e conceitos, como Grande Irmão, duplopensar, novidioma, buraco da memória e 2 2 5. O trabalho de Winston, o herói de 1984, é reescrever artigos de jornais do passado, de modo que o registro histórico sempre apoie a ideologia do Partido. Grande parte do Ministério também destrói os documentos que não foram revisados, dessa forma não há como provar que o governo esteja mentindo. Winston é um trabalhador diligente e habilidoso, mas odeia secretamente o Partido e sonha com a rebelião contra o Grande Irmão.',
        publisher: 'Principis',
        condition: 'Usado',
        volumes: '1 volume',
        price: 'R$15,00'
      }
    ]
}
  }





 