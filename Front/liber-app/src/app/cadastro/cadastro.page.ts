import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-cadastro',
  templateUrl: './cadastro.page.html',
  styleUrls: ['./cadastro.page.scss'],
})
export class CadastroPage implements OnInit {

  registerForm: FormGroup;

  constructor(public formbuilder: FormBuilder) { 
    this.registerForm = this.formbuilder.group({
      name: [null, [Validators.required, Validators.maxLength(20)]],
      password: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(8)]],
      email:[null,  [Validators.email, Validators.required]],
      phone:[null,  [Validators.required]],
      adress:[null, [Validators.required]],
      zipcode:[null,[Validators.required, Validators.maxLength(9),Validators.minLength(9)]]
    });
  }

  ngOnInit() {
  }


  submitForm(form){
    console.log(form);
    console.log(form.value);
  }


}

