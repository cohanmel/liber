import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-livro',
  templateUrl: './livro.component.html',
  styleUrls: ['./livro.component.scss'],
})
export class LivroComponent implements OnInit {
  
  @Input() livros;

  slideOpts = {slidesPerView: 3}

  constructor() { }

  ngOnInit() {}
  
}