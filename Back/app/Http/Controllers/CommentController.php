<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function create(Request $request){
        $comment = new Comment;
        $comment->user_id = $request->user_id;
        $comment->book_id = $request->book_id;
        $comment->text = $request->text;
        $comment->photo = $request->photo;
        $comment->avaliation = $request->avaliation;
        $comment->save();
        return response()->json(['comment' => $comment],200);

    }

    public function index(){
        $comment = Comment::all();
        return response()->json(['comment' => $comment],200);
    }


    public function show($id){
        $comment = Comment::find($id);
        return response()->json(['comment' => $comment],200);
    }

    public function update(Request $request, $id){
        $comment = Comment::find($id);
        if ($request->text){
            $comment->text = $request->text;
        }
        if ($request->photo){
            $comment->photo = $request->photo;
        }
        if ($request->avaliation){
            $comment->avaliation = $request->avaliation;
        }
        $comment->save();
        return response()->json(['comment' => $comment],200);

    }

    public function destroy($id){
        $comment = Comment::find($id);
        $comment->delete();
        return response()->json(['Comentário deletado com sucesso!'],200);
    }

}
