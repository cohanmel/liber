<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function create(Request $request){
        $user = new User;
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->photo = $request->photo;
        $user->adress = $request->adress;
        $user->password = $request->password;
        $user->save();
        return response()->json(['user' => $user],200);

    }

    public function index(){
        $user = User::all();
        return response()->json(['user' => $user],200);
    }


    public function show($id){
        $user = User::find($id);
        return response()->json(['user' => $user],200);
    }

    public function update(Request $request, $id){
        $user = User::find($id);
        if ($request->name){
            $user->name = $request->name;
        }
        if ($request->username){
            $user->username = $request->username;
        }
        if ($request->email){
            $user->email = $request->email;
        }
        if ($request->photo){
            $user->photo = $request->photo;
        }
        if ($request->adress){
            $user->adress = $request->adress;
        }
        if ($request->password){
            $user->password = $request->password;
        }
        $user->save();
        return response()->json(['user' => $user],200);

    }

    public function destroy($id){
        $user = User::find($id);
        $user->delete();
        return response()->json(['Perfil deletado com sucesso!'],200);
    }
}
