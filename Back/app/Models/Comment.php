<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Comment extends Model
{
    public function createComment(Request $request){
        $this->user_id = $request->user_id;
        $this->book_id = $request->book_id;
        $this->text = $request->text;
        $this->photo = $request->photo;
        $this->avaliation = $request->avaliation;
        $this->save();
    }

    public function updateComment(Request $request){
        if ($request->text){
            $this->text = $request->text;
        }
        if ($request->photo){
            $this->photo = $request->photo;
        }
        if ($request->avaliation){
            $this->avaliation = $request->avaliation;
        }
        $this->save();
    }

    public function users(){
        return $this->belongsTo('App\User');
    }

    public function books(){
        return $this->belongsTo('App\Comment');
    }
}