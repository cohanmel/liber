<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{   public function createUser(Request $request){
        $this->name = $request->name;
        $this->username = $request->username;
        $this->email = $request->email;
        $this->photo = $request->photo;
        $this->adress = $request->adress;
        $this->password = $request->password;
        $this->save();
    }

    public function updateUser(Request $request, $id){
        if ($request->name){
            $this->name = $request->name;
        }
        if ($request->username){
            $this->username = $request->username;
        }
        if ($request->email){
            $this->email = $request->email;
        }
        if ($request->photo){
            $this->photo = $request->photo;
        }
        if ($request->adress){
            $this->adress = $request->adress;
        }
        if ($request->password){
            $this->password = $request->password;
        }
        $this->save();
    }

    public function books(){
        return $this->hasMany('App\Book');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }




    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
