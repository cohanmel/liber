<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Book extends Model
{
    public function createBook(Request $request){
        $this->user_id = $request->user_id;
        $this->title = $request->title;
        $this->author = $request->author;
        $this->resume = $request->resume;
        $this->publisher = $request->publisher;
        $this->price = $request->price;
        $this->condition = $request->condition;
        $this->save();
    }

    public function updateBook(Request $request, $id){
        if ($request->title){
            $this->title = $request->title;
        }
        if ($request->author){
            $this->author = $request->author;
        }
        if ($request->resume){
            $this->resume = $request->resume;
        }
        if ($request->publisher){
            $this->publisher = $request->publisher;
        }
        if ($request->price){
            $this->price = $request->price;
        }
        if ($request->condition){
            $this->condition = $request->condition;
        }
        $this->save();
    }

    public function users(){
        return $this->belongsTo('App\User');
    }


}

